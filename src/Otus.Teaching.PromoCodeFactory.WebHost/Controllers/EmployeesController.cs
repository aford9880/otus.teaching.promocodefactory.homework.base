﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание нового сотрудника, необходимо использовать существующие ID ролей
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns>Ок если создание успешно и forbid если не хватает данных</returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            // Создание сотрудника на основе данных из запроса
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                Roles = _roleRepository.GetByListIdAsync(employeeRequest.RoleIds).Result.ToList(),
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };
            if (employee.Roles.Count == 0)
                return BadRequest("invalid role(s) for new employee");
            // Добавляем нового сотрудника в список
            await _employeeRepository.AddAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <param name="employeeRequest">Данные по сотруднику</param>
        /// <returns>ОК если все ок, Not found если не нашли сотрудника по Id и BadRequest в случае ошибки валидации</returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            var existingEmployee = await _employeeRepository.GetByIdAsync(id);

            if (existingEmployee == null)
                return NotFound();

            existingEmployee.FirstName = employeeRequest.FirstName;
            existingEmployee.LastName = employeeRequest.LastName;
            existingEmployee.Email = employeeRequest.Email;
            existingEmployee.Roles = _roleRepository.GetByListIdAsync(employeeRequest.RoleIds).Result.ToList();
            existingEmployee.AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount;

            if (existingEmployee.Roles.Count == 0)
                return BadRequest("invalid role(s) for employee");

            await _employeeRepository.UpdateAsync(existingEmployee);

            return Ok();
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id">Guid</param>
        /// <returns>Ок при успешном удалении</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.DelAsync(id);

            return Ok();
        }
    }
}