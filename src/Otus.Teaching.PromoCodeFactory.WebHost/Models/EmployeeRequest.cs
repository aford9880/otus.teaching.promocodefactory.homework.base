﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        
        public IEnumerable<Guid> RoleIds { get; set; }

        [PositiveNumber(ErrorMessage = "Promocodes count is required and must be more or equal 0")]
        public int AppliedPromocodesCount { get; set; }
    }

    public class PositiveNumberAttribute: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int intValue;
            if (value != null && int.TryParse(value.ToString(), out intValue))
            {
                return intValue >= 0;
            }
            return false;
        }
    }
}
