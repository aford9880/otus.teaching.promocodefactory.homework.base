﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Добавление сотрудника
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> DelAsync(Guid id);

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<T> UpdateAsync(T entity);
        
        /// <summary>
        /// Достаем сущностей по списку ID
        /// </summary>
        /// <param name="roleIds">Перечисление guid-ов</param>
        /// <returns>Список ролей</returns>
        Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> roleIds);
    }
}