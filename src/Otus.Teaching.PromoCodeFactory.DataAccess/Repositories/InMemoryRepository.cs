﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        /// <summary>
        /// Добавление нового элемента в конец коллекции
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Task<T> AddAsync(T entity)
        {
            Data = Data.Concat(new[] { entity });
            return Task.FromResult(entity);
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<T> DelAsync(Guid id)
        {
            var entity = Data.FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                return Task.FromResult<T>(null);
            }
            Data = Data.Where(x => x.Id != id);
            return Task.FromResult(entity);
        }

        /// <summary>
        /// Обновление элемента
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task<T> UpdateAsync(T entity)
        {
            var existingEntity = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (existingEntity== null)
            {
                throw new InvalidOperationException("Entity not found");
            }

            Data = Data.Select(x => x.Id == entity.Id ? entity : x);
            return Task.FromResult(entity);
        }

        /// <summary>
        /// Достаем сущности по списку ID
        /// </summary>
        /// <param name="roleIds">Перечисление ID</param>
        /// <returns>Список ролей</returns>
        public Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> roleIds)
        {
            var result = Data.Where(x => x is Role role && roleIds.Contains(role.Id));
            return Task.FromResult(result);
        }
    }
}